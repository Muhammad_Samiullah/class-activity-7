package com.example.classactivity7;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.text.DecimalFormat;

public class ShowLocation extends AppCompatActivity {
    private GpsTracker gpsTracker;
    DecimalFormat df = new DecimalFormat("###.#######");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_location);
        gpsTracker = new GpsTracker(this);

        if(gpsTracker.canGetLocation())
        {
            double latitude = gpsTracker.getLatitude();
            double longitude=gpsTracker.getLongitude();
            TextView textViewLatitude = (TextView)findViewById(R.id.textViewLatitude);
            textViewLatitude.setText("Latitude: " + df.format(latitude));


            TextView textViewLongitude = (TextView)findViewById(R.id.textViewLongitude);
            textViewLongitude.setText("Longitude: " +df.format(longitude));

        }
    }
}